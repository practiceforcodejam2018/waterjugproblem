#include <iostream>

using namespace std;

typedef struct water_jug_structure
{
    int jugCapacity;
    int jugCurrentWaterRetention;
}water_jug_struct;


class GCDCalculator
{
    public:
        int getGCDOfElements(int num1 , int num2)
        {
            if (!(num1 % num2))
            {
                return num2;
            }
            return getGCDOfElements(num2 , (num1 % num2));
        }
};


class WaterJug
{
    private:
        water_jug_struct jugA;
        water_jug_struct jugB;
    
        void initializeJugsWithMaxCapacities(int maxCapacityForJugA , int maxCapacityForJugB)
        {
            jugA.jugCapacity              = maxCapacityForJugA;
            jugA.jugCurrentWaterRetention = 0;
            jugB.jugCapacity              = maxCapacityForJugB;
            jugB.jugCurrentWaterRetention = 0;
        }
    
        void displayCurrentStatusOfJugs()
        {
            cout<<"["<<jugA.jugCurrentWaterRetention<<" , "<<jugB.jugCurrentWaterRetention<<"]"<<endl;
        }
    
        void fillJugA()
        {
            jugA.jugCurrentWaterRetention = jugA.jugCapacity;
            displayCurrentStatusOfJugs();
        }
    
        void fillJugB()
        {
            jugB.jugCurrentWaterRetention = jugB.jugCapacity;
            displayCurrentStatusOfJugs();
        }
    
        void emptyJugA()
        {
            jugA.jugCurrentWaterRetention = 0;
            displayCurrentStatusOfJugs();
        }
    
        void emptyJugB()
        {
            jugB.jugCurrentWaterRetention = 0;
            displayCurrentStatusOfJugs();
        }
    
        void transferWaterFromJugAToJugB()
        {
            int remainingCapacityInJugB = jugB.jugCapacity - jugB.jugCurrentWaterRetention;
            if(remainingCapacityInJugB >= jugA.jugCurrentWaterRetention)
            {
                jugB.jugCurrentWaterRetention += jugA.jugCurrentWaterRetention;
                jugA.jugCurrentWaterRetention  = 0;
            }
            else
            {
                jugA.jugCurrentWaterRetention -= remainingCapacityInJugB;
                jugB.jugCurrentWaterRetention  = jugB.jugCapacity;
            }
            displayCurrentStatusOfJugs();
        }
    
    public:
        void findSolutionToWaterJugProblemWithJugCapacitiesAndGoal(int jugACapacity , int jugBCapacity , int goal)
        {
            GCDCalculator gcdCalculator              = GCDCalculator();
            int           gcdOfJugAAndJugBCapacities = gcdCalculator.getGCDOfElements(jugACapacity , jugBCapacity);
            
            if(!(goal % gcdOfJugAAndJugBCapacities))
            {
                initializeJugsWithMaxCapacities(jugACapacity , jugBCapacity);
                bool isGoalAchieved = false;
                while (!isGoalAchieved)
                {
                    if(jugA.jugCurrentWaterRetention == goal || jugB.jugCurrentWaterRetention == goal)
                    {
                        isGoalAchieved = true;
                    }
                    
                    if(jugA.jugCurrentWaterRetention == 0)
                    {
                        fillJugA();
                    }
                    else if(jugA.jugCurrentWaterRetention > 0 && jugB.jugCurrentWaterRetention != jugB.jugCapacity)
                    {
                        transferWaterFromJugAToJugB();
                    }
                    else if(jugA.jugCurrentWaterRetention > 0 && jugB.jugCurrentWaterRetention == jugB.jugCapacity)
                    {
                        emptyJugB();
                    }
                }
            }
            else
            {
                cout<<"This can't be done..."<<endl;
            }
        }
};

int main(int argc, const char * argv[])
{
    WaterJug waterJug = WaterJug();
    waterJug.findSolutionToWaterJugProblemWithJugCapacitiesAndGoal(6 , 9 , 3);
    return 0;
}
